import * as functions from 'firebase-functions';
const express = require('express')
import * as cookieParser from "cookie-parser"
import * as cors from 'cors'

import appAfterAuth from "./appAfterAuth/appAfterAuth";
import appBeforeAuth from "./appBeforeAuth/appBeforeAuth";
import admin from "./firebaseAdmin"

// Express instances
const main = express();

main.use(cors({origin:true}))

main.use('/authApi', appBeforeAuth)
main.use('/postAuthApi', appAfterAuth)

main.use(cookieParser)

const db = admin.firestore()
//const auth = admin.auth()

export const companyApis = functions.https.onRequest(main);

export const saveNewUser = functions.auth.user().onCreate((userRecord) => {
    const usersDoc = db.doc("users/" + userRecord.uid)
    const details = {
        uid:userRecord.uid === undefined? null: userRecord.uid,
        phoneNumber:userRecord.phoneNumber === undefined? null: userRecord.phoneNumber,
        email: userRecord.email === undefined? null: userRecord.email,
        displayName:userRecord.displayName === undefined? null: userRecord.displayName,
        isEmailVerified:userRecord.emailVerified === undefined? null: userRecord.emailVerified,
        photoUrl:userRecord.photoURL === undefined? null: userRecord.photoURL,
        creationTime: userRecord.metadata.creationTime === undefined? null: userRecord.metadata.creationTime,
        lastSignInTime:userRecord.metadata.lastSignInTime === undefined? null: userRecord.metadata.lastSignInTime,
        requestedKYC:false,
        approvedKYC:false,
        noOfBooks: 0,
        shopAddress: null
    }
    usersDoc.set(details, { merge: true }).then(function (result) {
        console.log("success " + result)
    }).catch(function (error) {
        console.log("Error occurred " + error)
    })
})


