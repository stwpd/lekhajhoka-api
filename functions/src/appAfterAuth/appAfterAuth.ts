import validateFirebaseIdToken from "./validateFirebaseIdToken";
import {Request, Response} from "express";
import updateUserData from "./updateUserData";
import saveNewUserInDb from "./saveNewUserInDb";
import kycDetailsUpload from "./kycDetailsUpload";

import * as bodyParser from "body-parser";
import * as multer from "multer"
import updateSessionDoc from "./updateSessionDoc";
import getFirebaseRecord from "./getFirestoreRecord";
import makeNewBook from "./makeNewBook";
import makeNewCustomersRecord from "./makeNewCustomersRecord";
import makeNewCustomer from "./makeNewCustomer";
import makeNewLoanRecord from "./makeNewLoanRecord";
import makeNewLoan from "./makeNewLoan";

const express = require('express')

// express instance of after auth middleware
const appAfterAuth = express();

//Body Parser which will parse json body of request;
//Use it with every route which has json body in the request
//Use it as a middleware
const jsonParser = bodyParser.json()

const storage = multer.diskStorage({
    destination: function (req:Request, file:any, cb:any) {
        cb(null, 'uploads/')
    },
    filename: function (req:Request, file:any, cb:any) {
        cb(null, file.originalname)
    }
})
const uploadParser = multer({storage: storage})

// Middleware to validate Firebase Id token; This ensures the security of api of this route
appAfterAuth.use(validateFirebaseIdToken)

//api to check this route
//no Parser is needed
appAfterAuth.get("/checkPostAuth", async(req : Request, res : Response) => {
    try {
        res.send("check complete")
    }
    catch(error) {
        res.send("error occurred " + error)
    }
})

//api to update the personal details of user
// json Parser is needed
appAfterAuth.post("/updateUserData", jsonParser, updateUserData)

//api to save new user in firestore after successful login
// json Parser is needed
appAfterAuth.post("/saveNewUserInDb", jsonParser, saveNewUserInDb)

//api to upload details and docs for kyc
//upload parser is needed (files parser of multer)
appAfterAuth.post("/kycDetailsUpload", uploadParser.array('kycDocs', 12), kycDetailsUpload)

//api to update session doc
//json Parser is needed
appAfterAuth.post("/updateSessionDoc", jsonParser, updateSessionDoc)

//api to any any document from firestore
//json Parser is needed
appAfterAuth.post("/getFirestoreDocument", jsonParser, getFirebaseRecord)

//api make a new book
// json Parser is needed
appAfterAuth.post("/makeNewBook", jsonParser, makeNewBook)

//api make a new customer record (without loan)
// json Parser is needed
appAfterAuth.post("/makeNewCustomerRecord", jsonParser, makeNewCustomersRecord)

//api make a new customer
// json Parser is needed
appAfterAuth.post("/makeNewCustomer", jsonParser, makeNewCustomer)

//api make a new loan record
// json Parser is needed
appAfterAuth.post("/makeNewLoanRecord", jsonParser, makeNewLoanRecord)

//api make a new loan
// json Parser is needed
appAfterAuth.post("/makeNewLoan", jsonParser, makeNewLoan)

export default appAfterAuth
