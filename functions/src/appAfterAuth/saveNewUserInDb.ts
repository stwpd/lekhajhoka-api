import {Request, Response} from "express";
import admin from "../firebaseAdmin"

const auth = admin.auth()
const db = admin.firestore()

interface customRequest extends Request {
    user: any
}

function createUserInDb(userRecord:any, uid:string, res:Response) {
    const docDetails = {
        uid:userRecord.uid === undefined? null: userRecord.uid,
        phoneNumber:userRecord.phoneNumber === undefined? null: userRecord.phoneNumber,
        email: userRecord.email === undefined? null: userRecord.email,
        lastSignInTime:userRecord.lastSignInTime === undefined? null: userRecord.lastSignInTime,
        isEmailVerified:userRecord.isEmailVerified === undefined? null: userRecord.isEmailVerified,
        creationTime:userRecord.creationTime === undefined? null: userRecord.creationTime

    }

    const usersDoc = db.doc("users/" + uid)
    usersDoc.set(docDetails, { merge: true }).then(function (result) {
        res.status(200).send(result)
    }).catch(function (error) {
        res.status(400).send(error)
    })
}

const saveNewUserInDb = async (req : customRequest, res : Response) => {
    try {
        const uid = req.user['uid']
        auth.getUser(uid)
            .then(function(userRecord) {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully fetched user data:', userRecord.toJSON());

                createUserInDb(userRecord.toJSON(), uid, res)

            })
            .catch(function(error) {
                console.log('Error fetching user data:', error);
                res.status(400).send(uid + " Some error came " + error)
            });

    } catch (error) {
        res.status(400).send('Sorry! Some went wrong at our side. Please try again')
    }
}

export default saveNewUserInDb

