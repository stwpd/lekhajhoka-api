import {Request, Response} from "express";
import admin from "../firebaseAdmin"

import * as fs from "fs"

//const auth = admin.auth()
const storageBucket = admin.storage().bucket()
const db = admin.firestore()

interface uploadResponse{
    writeTime:any,
    isSuccess:boolean,
    isError:boolean,
    error: string,
    statusCode:number
}

interface KYCDetails {
    requestedKYC:boolean,
    approvedKYC:boolean
}

interface customRequest extends Request{
    user:any
    files:any
}

const kycDetailsUpload = async (req : customRequest, res : Response) => {
    try {
        const files = req.files
        const uid = req.user['uid']
        if (!files) {
            const u_response: uploadResponse = {
                writeTime:null,
                isSuccess:false,
                isError:true,
                error:"no file found",
                statusCode:400
            }
            res.send(u_response)
            return
        }

        //upload files in firebase Storage
        uploadFiles(files, uid)

        //delete files from upload folder
        deleteFiles(files)

        const writeTime = await updateFirestore(uid)

        const response: uploadResponse = {
            writeTime:writeTime,
            isSuccess:true,
            isError:false,
            error:'',
            statusCode:200
        }

        res.status(200).send(response)

    } catch (error) {
        const response: uploadResponse = {
            writeTime:null,
            isSuccess:false,
            isError:true,
            error:error,
            statusCode:400
        }
        res.send(response)
    }
}

function uploadFiles(files:any, uid:string) {

    files.forEach(function (file:any) {
        const storageOptions = {
            destination: "KYC_DOCS/" + uid + "/"+ file.originalname,
            gzip: true,
        }
        storageBucket.upload(file.path, storageOptions, uploadCallback)
    })
}

function uploadCallback(err:any) {
    if(!err) {
        //done


    }
}

function deleteFiles(files:any) {
    files.forEach(function (file:any) {
        try {
            fs.unlinkSync(file.path)
        }
        catch (e) {
            console.log(e)
        }
    })
}

function updateFirestore(uid:string) {
    return new Promise((resolve,reject) => {
        const usersDoc = db.doc("users/" + uid)

        const data:KYCDetails = {
            requestedKYC:true,
            approvedKYC:false
        }

        usersDoc.update(data)
            .then(function (result) {
                const writeTime = result.writeTime
                resolve(writeTime)
            })
            .catch(function (e) {
                reject(e)
            })
    })
}

export default kycDetailsUpload
