import {Request, Response} from "express";
import admin from "../firebaseAdmin"

const db = admin.firestore()

interface customRequest extends Request {
    user: any
}

interface updateResponse {
    loanRecordRemoteId: string,
    isError:boolean,
    error: string
    statusCode:number
}

const makeNewLoanRecord = async (req : customRequest, res : Response) => {
    const recordData = req.body
    const localId = recordData.localId
    const uid = req.user.uid
    try {

        const loanRecordsCollectionRef: admin.firestore.CollectionReference = db.collection("users/" + uid + "/loanRecords")
        const bookRes = await loanRecordsCollectionRef.add({
            localBookId: recordData.localBookId,
            remoteBookId: recordData.remoteBookId,
            amount: recordData.amount,
            date: recordData.date,
            dueDate: recordData.dueDate,
            remarks: recordData.remarks,
            give: recordData.give,
            take: recordData.take,
            attachment: recordData.attachment,
            partnerContact: recordData.partnerContact,
            phoneId: recordData.phoneId,
            type: recordData.type,
            interest: recordData.interest,
            installments: recordData.installments,
            totalMonths: recordData.totalMonths,
            loanName: recordData.loanName,
            installmentAmount: recordData.installmentAmount,
            localId,
            creationTime: admin.firestore.FieldValue.serverTimestamp()
        })

        const response: updateResponse = {
            loanRecordRemoteId: bookRes.id,
            isError:false,
            error: "",
            statusCode:200
        }

        res.send(response)

    } catch (error) {

        const response: updateResponse = {
            loanRecordRemoteId: "",
            isError:true,
            error: error,
            statusCode:400
        }

        res.send(response)
    }
}

export default makeNewLoanRecord
