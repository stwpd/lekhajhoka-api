import {Request, Response} from "express";
import updateFirestore from "../Logics/updateFirestore";

interface updateResponse {
    writeTime:any,
    isUpdated: boolean,
    isError:boolean,
    error: string
    statusCode:number
}

interface customRequest extends Request {
    user: any
}

const updateSessionDoc = async (req : customRequest, res : Response) => {
    const uid = req.user['uid']
    const sessionId = req.body['sessionId']
    let idToken
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        idToken = req.headers.authorization.split('Bearer ')[1]
    }

    try {

        const WriteRes:any = await updateFirestore({userUid:uid, token:idToken}, 'sessions/' + sessionId)

        const response: updateResponse = {
            writeTime:WriteRes,
            isUpdated: true,
            isError:false,
            error: '',
            statusCode:200
        }

        res.send(response)

    } catch (error) {

        const response: updateResponse = {
            isUpdated: false,
            isError:true,
            error: error,
            writeTime:null,
            statusCode:400
        }

        res.send(response)
    }
}


export default updateSessionDoc
