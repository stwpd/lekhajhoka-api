import {Request, Response} from "express";
import admin from "../firebaseAdmin"

const db = admin.firestore()

interface customRequest extends Request {
    user: any
}

interface updateResponse {
    recordRemoteId: string,
    isError:boolean,
    error: string
    statusCode:number
}

const makeNewCustomersRecord = async (req : customRequest, res : Response) => {
    const recordData = req.body
    const localId = recordData.localId
    const uid = req.user.uid
    try {

        const customersRecordsCollectionRef: admin.firestore.CollectionReference = db.collection("users/" + uid + "/customersRecords")
        const bookRes = await customersRecordsCollectionRef.add({
            localBookId: recordData.localBookId,
            remoteBookId: recordData.remoteBookId,
            amount: recordData.amount,
            date: recordData.date,
            dueDate: recordData.dueDate,
            remarks: recordData.remarks,
            give: recordData.give,
            take: recordData.take,
            attachment: recordData.attachment,
            partnerContact: recordData.partnerContact,
            phoneId: recordData.phoneId,
            type: recordData.type,
            localId,
            creationTime: admin.firestore.FieldValue.serverTimestamp()
        })

        const response: updateResponse = {
            recordRemoteId: bookRes.id,
            isError:false,
            error: "",
            statusCode:200
        }

        res.send(response)

    } catch (error) {

        const response: updateResponse = {
            recordRemoteId: "",
            isError:true,
            error: error,
            statusCode:400
        }

        res.send(response)
    }
}

export default makeNewCustomersRecord
