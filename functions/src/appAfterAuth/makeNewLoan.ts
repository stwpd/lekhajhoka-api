import {Request, Response} from "express";
import admin from "../firebaseAdmin"

const db = admin.firestore()

interface customRequest extends Request {
    user: any
}

interface updateResponse {
    loanRemoteId: string,
    isError:boolean,
    error: string
    statusCode:number
}

const makeNewLoan = async (req : customRequest, res : Response) => {
    const recordData = req.body
    const localId = recordData.localId
    const uid = req.user.uid
    try {

        const loansCollectionRef: admin.firestore.CollectionReference = db.collection("users/" + uid + "/loans")
        const remoteRes = await loansCollectionRef.add({
            contactNo: recordData.contactNo,
            loanName: recordData.loanName,
            lastUpdated: recordData.lastUpdated,
            contactName: recordData.contactName,
            isActive: recordData.isActive,
            localId,
            creationTime: admin.firestore.FieldValue.serverTimestamp()
        })

        const response: updateResponse = {
            loanRemoteId: remoteRes.id,
            isError:false,
            error: "",
            statusCode:200
        }

        res.send(response)

    } catch (error) {

        const response: updateResponse = {
            loanRemoteId: "",
            isError:true,
            error: error,
            statusCode:400
        }

        res.send(response)
    }
}

export default makeNewLoan
