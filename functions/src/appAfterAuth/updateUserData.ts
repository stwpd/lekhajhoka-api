import {Request, Response} from "express";
import admin from "../firebaseAdmin"
import updateFirestore from "../Logics/updateFirestore";

const auth = admin.auth()

interface customReq extends Request {
    user: any
}

interface updateResponse {
    writeTime:any,
    userRecord:object,
    isUpdated: boolean,
    isError:boolean,
    error: string
    statusCode:number
}

const updateUserData = async (req : customReq, res : Response) => {

    const uid = req.user['uid']
    const updateAuth = req.body['updateAuth']

    let userRecord = {}

    try {

        const ph_no = req.body['phoneNumber']
        const photo_url = req.body['photoUrl']
        const email = req.body['email']
        const name = req.body['displayName']

        let WriteRes:any

        if(updateAuth) {
            userRecord = await auth.updateUser(uid, {
                email: email,
                phoneNumber: ph_no,
                displayName: name,
                photoURL: photo_url,
            })

            WriteRes = await updateFirestore(userRecord, "users/" + uid)
        }
        else {
            // tslint:disable-next-line:no-shadowed-variable
            const {updateAuth, ...records} = req.body;
            WriteRes = await updateFirestore(records, "users/" + uid)
        }

        const response: updateResponse = {
            writeTime:WriteRes,
            userRecord: userRecord,
            isUpdated: true,
            isError:false,
            error: '',
            statusCode:200
        }

        res.send(response)

    } catch (error) {

        const response: updateResponse = {
            userRecord: userRecord,
            isUpdated: false,
            isError:true,
            error: error,
            writeTime:null,
            statusCode:400
        }

        res.send(response)
    }
}

export default updateUserData

