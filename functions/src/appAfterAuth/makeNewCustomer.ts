import {Request, Response} from "express";
import admin from "../firebaseAdmin"

const db = admin.firestore()

interface customRequest extends Request {
    user: any
}

interface updateResponse {
    customersRemoteId: string,
    isError:boolean,
    error: string
    statusCode:number
}

const makeNewCustomer = async (req : customRequest, res : Response) => {
    const recordData = req.body
    const localId = recordData.localId
    const uid = req.user.uid
    try {

        const customersCollectionRef: admin.firestore.CollectionReference = db.collection("users/" + uid + "/customers")
        const bookRes = await customersCollectionRef.add({
            localBookId: recordData.localBookId,
            remoteBookId: recordData.remoteBookId,
            contactNo: recordData.contactNo,
            name: recordData.name,
            lastUpdated: recordData.lastUpdated,
            loanYes: recordData.loanYes,
            address: recordData.address,
            receivableAmount: recordData.receivableAmount,
            payableAmount: recordData.payableAmount,
            netAmount: recordData.netAmount,
            localId,
            creationTime: admin.firestore.FieldValue.serverTimestamp()
        })

        const response: updateResponse = {
            customersRemoteId: bookRes.id,
            isError:false,
            error: "",
            statusCode:200
        }

        res.send(response)

    } catch (error) {

        const response: updateResponse = {
            customersRemoteId: "",
            isError:true,
            error: error,
            statusCode:400
        }

        res.send(response)
    }
}

export default makeNewCustomer
