import {Request, Response} from "express";
import admin from "../firebaseAdmin"

const db = admin.firestore()

interface customRequest extends Request {
    user: any
}

interface updateResponse {
    bookRemoteId: string,
    isError:boolean,
    error: string
    statusCode:number
}

const makeNewBook = async (req : customRequest, res : Response) => {
    const bookName = req.body['bookName']
    const localId = req.body['localId']
    const uid = req.user.uid
    try {

        const booksCollectionRef: admin.firestore.CollectionReference = db.collection("users/" + uid + "/books")
        const bookRes = await booksCollectionRef.add({
            bookName,
            localId,
            creationTime: admin.firestore.FieldValue.serverTimestamp()
        })

        const response: updateResponse = {
            bookRemoteId: bookRes.id,
            isError:false,
            error: "",
            statusCode:200
        }

        res.send(response)

    } catch (error) {

        const response: updateResponse = {
            bookRemoteId: "",
            isError:true,
            error: error,
            statusCode:400
        }

        res.send(response)
    }
}

export default makeNewBook
