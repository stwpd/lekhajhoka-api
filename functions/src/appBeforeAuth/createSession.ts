import {Request, Response} from "express";
import admin from "../firebaseAdmin";
import Timestamp = admin.firestore.Timestamp;

const db = admin.firestore()

interface statusResponse {
    sessionId: string,
    isError:boolean,
    error: string,
    statusCode:number
}

interface docData {
    creationDate:Timestamp
    userUid:any
}

const createSession = async (req : Request, res : Response) => {
    try {
        //create document in firestore in sessions collection
        const sessionRef = db.collection('sessions')

        const data:docData = {
            creationDate: admin.firestore.Timestamp.now(),
            userUid:null
        }

        const result = await sessionRef.add(data)

        //send session_id in response
        const mResponse:statusResponse = {
            sessionId:result.id,
            isError:false,
            error:'',
            statusCode:200
        }
        res.send(mResponse)

    } catch (error) {
        const mResponse:statusResponse = {
            sessionId:'',
            isError:true,
            error:error,
            statusCode:400
        }
        res.send(mResponse)
    }
}

export default createSession
