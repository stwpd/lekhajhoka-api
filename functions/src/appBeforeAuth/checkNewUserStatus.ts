import {Request, Response} from "express";
import admin from "../firebaseAdmin"

const auth = admin.auth()

interface statusResponse {
    isNewUser: boolean,
    isError:boolean,
    error: string
}

const checkNewUserStatus = async (req : Request, res : Response) => {
    try {
        const ph_no = req.body['ph_no']

        auth.getUserByPhoneNumber(ph_no)
            .then(function(userRecord) {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully fetched user data:', userRecord.toJSON());

                const response: statusResponse = {
                    isNewUser: false,
                    isError:false,
                    error: "no error"
                }

                res.send(response)
            })
            .catch(function(error) {
                console.log('Error fetching user data:', error);

                const response: statusResponse = {
                    isNewUser: true,
                    isError:true,
                    error: error.errorCode + error.error
                }

                res.send(response)

            });

    } catch (error) {
        res.status(400).send('Sorry! Some went wrong at our side. Please try again')
    }
}

export default checkNewUserStatus
