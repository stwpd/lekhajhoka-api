import checkNewUserStatus from "./checkNewUserStatus";
import createSession from "./createSession";
import * as bodyParser from "body-parser"
const express = require('express')

const appBeforeAuth = express();

const jsonParser = bodyParser.json()
//const urlEncodedParser = bodyParser.urlencoded({ extended: false })

//json parser is needed
appBeforeAuth.post('/checkNewUserStatus', jsonParser, checkNewUserStatus)

appBeforeAuth.get('/createSession', createSession)

export default appBeforeAuth
